<?php
require_once('connection.php');
require_once('main_function.php');
$_POST = json_decode(file_get_contents("php://input"),true);
$exp_country = $_POST['country'];
$year = $_POST['year'];
// $exp_country = 'ARG';
// $year = 2017;


//set 9
//get region
$region_data = $db->select("country_list","region",[
iso =>$exp_country
]);
$region = $region_data[0];

//get country in same region
$country_data = $db->select("country_list","iso",[
region =>$region
]);
$result = array();


for($i=0; $i<count($country_data);$i++){
   
    $result[$country_data[$i]]['latinAmerica']['value'] = 0;
    $result[$country_data[$i]]['latinAmerica']['ratio'] = 0;
    $result[$country_data[$i]]['asiaPacific']['value'] =0;
    $result[$country_data[$i]]['asiaPacific']['ratio']  =0;
    $result[$country_data[$i]]['europe']['value'] = 0;
    $result[$country_data[$i]]['europe']['ratio'] = 0;
    $result[$country_data[$i]]['northAmerica']['value'] = 0;
    $result[$country_data[$i]]['northAmerica']['ratio'] = 0;
    $result[$country_data[$i]]['row']['value'] =0;
    $result[$country_data[$i]]['row']['ratio'] =0;
    $tableName = strtolower($country_data[$i]) . "_" . $year;
    $sql  = "select sum(value) as sum, source_country  from " . $tableName . "  
  where variable = 'fva_yl' and ( imp_country NOT IN ('sea', 'nca', 'sswa', 'enea', 'pac', 'ap', 'euz', 'eur', 'apta', 'saarc', 'nafta', 'mercosur', 'cptpp', 'rcep', 'apec', 'lac', 'pac_alliance', 'fealac', 'bimstec', 'wld'))   group by source_country" ;
  $value1 = $db->query($sql)->fetchAll();

  
    //วนใส่ region
    for($j=0;$j<count($value1);$j++){
        if($value1[$j]['source_country'] != $country_data[$i]){
 $country_data2 = $db->select("country_list","area",[iso=>$value1[$j]['source_country']]);
 
        if($country_data2[0] == 'Latin America'){
            $result[$country_data[$i]]['latinAmerica']['value'] += round($value1[$j]['sum'],2);
        } else if($country_data2[0] == 'Asia-Pacific'){
            $result[$country_data[$i]]['asiaPacific']['value']+= $value1[$j]['sum'];
        } else if($country_data2[0] == 'Europe'){
            $result[$country_data[$i]]['europe']['value']+= $value1[$j]['sum'];
        }else if($country_data2[0] == 'North America'){
            $result[$country_data[$i]]['northAmerica']['value']+= $value1[$j]['sum'];
        } else if($country_data2[0] == 'Rest of the world'){
            $result[$country_data[$i]]['row']['value']+= $value1[$j]['sum'];
        }

        }    
    }

    $sql  = "select sum(value) as sum  from " . $tableName . "  
  where variable = 'total_export' and ( imp_country NOT IN ('sea', 'nca', 'sswa', 'enea', 'pac', 'ap', 'euz', 'eur', 'apta', 'saarc', 'nafta', 'mercosur', 'cptpp', 'rcep', 'apec', 'lac', 'pac_alliance', 'fealac', 'bimstec', 'wld')) " ;
   $value2 = $db->query($sql)->fetchAll();
   $result[$country_data[$i]]['total'] = round($value2[0]['sum'],2);
   $result[$country_data[$i]]['latinAmerica']['value'] = round($result[$country_data[$i]]['latinAmerica']['value'],2);
   $result[$country_data[$i]]['asiaPacific']['value'] = round($result[$country_data[$i]]['asiaPacific']['value'],2);
   $result[$country_data[$i]]['europe']['value'] = round($result[$country_data[$i]]['europe']['value'],2);
    $result[$country_data[$i]]['northAmerica']['value'] = round($result[$country_data[$i]]['northAmerica']['value'],2);
    $result[$country_data[$i]]['row']['value'] = round($result[$country_data[$i]]['row']['value'],2);

   $result[$country_data[$i]]['latinAmerica']['ratio'] =  round($result[$country_data[$i]]['latinAmerica']['value'] / $value2[0]['sum']*100,4);
   $result[$country_data[$i]]['asiaPacific']['ratio'] =  round($result[$country_data[$i]]['asiaPacific']['value'] / $value2[0]['sum']*100,4);
   $result[$country_data[$i]]['europe']['ratio'] =  round($result[$country_data[$i]]['europe']['value'] / $value2[0]['sum']*100,4);
   $result[$country_data[$i]]['northAmerica']['ratio'] =  round($result[$country_data[$i]]['northAmerica']['value'] / $value2[0]['sum']*100,4);
$result[$country_data[$i]]['row']['ratio'] =  round($result[$country_data[$i]]['row']['value'] / $value2[0]['sum']*100,4);


 


}
// echo "\n**********9********\n";
//   echo json_encode($result);
   $set9 = json_encode($result);
// $db->update("country_brief",["set9"=>$dataInput],["AND"=>["economy"=>$exp_country,"year"=>$year]]);


//set 9a
//get region
// $region_data = $db->select("country_list","region",[
// iso =>$exp_country
// ]);
// $region = $region_data[0];

// //get country in same region
// $country_data = $db->select("country_list","iso",[
// region =>$region
// ]);
$result = array();
$result2 = array();

$result['latinAmerica']['value'] = 0;
$result['asiaPacific']['value'] =0;
$result['europe']['value'] = 0;
$result['northAmerica']['value'] = 0;
$result['row']['value'] =0;
 $result['total']  = 0;

for($i=0; $i<count($country_data);$i++){
    $tableName = strtolower($country_data[$i]) . "_" . $year;
    $sql  = "select sum(value) as sum, source_country  from " . $tableName . "  
  where variable = 'fva_yl'  and ( imp_country NOT IN ('sea', 'nca', 'sswa', 'enea', 'pac', 'ap', 'euz', 'eur', 'apta', 'saarc', 'nafta', 'mercosur', 'cptpp', 'rcep', 'apec', 'lac', 'pac_alliance', 'fealac', 'bimstec', 'wld'))  group by source_country" ;
  $value1 = $db->query($sql)->fetchAll();

      //วนใส่ region
    for($j=0;$j<count($value1);$j++){
       
 $country_data2 = $db->select("country_list","area",[iso=>$value1[$j]['source_country']]);
 
        if($country_data2[0] == 'Latin America'){
            $result['latinAmerica']['value'] += round($value1[$j]['sum'],2);
        } else if($country_data2[0] == 'Asia-Pacific'){
            $result['asiaPacific']['value']+= $value1[$j]['sum'];
        } else if($country_data2[0] == 'Europe'){
            $result['europe']['value']+= $value1[$j]['sum'];
        }else if($country_data2[0] == 'North America'){
            $result['northAmerica']['value']+= $value1[$j]['sum'];
        } else if($country_data2[0] == 'Rest of the world'){
            $result['row']['value']+= $value1[$j]['sum'];
        }

          
    }

    $sql  = "select sum(value) as sum  from " . $tableName . " where variable = 'total_export'  and ( imp_country NOT IN ('sea', 'nca', 'sswa', 'enea', 'pac', 'ap', 'euz', 'eur', 'apta', 'saarc', 'nafta', 'mercosur', 'cptpp', 'rcep', 'apec', 'lac', 'pac_alliance', 'fealac', 'bimstec', 'wld')) " ;
   $value2 = $db->query($sql)->fetchAll();
   $result['total'] += round($value2[0]['sum'],2);
}

$result2['asiaPacific']['value'] = round($result['asiaPacific']['value'],2);
$result2['asiaPacific']['ratio'] = round($result['asiaPacific']['value'] / $result['total']*100,2);

$result2['latinAmerica']['value'] = round($result['latinAmerica']['value'],2);
$result2['latinAmerica']['ratio'] = round($result['latinAmerica']['value'] / $result['total']*100,2);

$result2['europe']['value'] = round($result['europe']['value'],2);
$result2['europe']['ratio'] = round($result['europe']['value'] / $result['total']*100,2);

$result2['northAmerica']['value'] = round($result['northAmerica']['value'],2);
$result2['northAmerica']['ratio'] = round($result['northAmerica']['value'] / $result['total']*100,2);


$result2['row']['value'] = round($result['row']['value'],2);
$result2['row']['ratio'] = round($result['row']['value'] / $result['total']*100,2);

// echo "\n**********9A********\n";
//   echo json_encode($result2);

$set9a = json_encode($result2);


//set 10
//get region
// $region_data = $db->select("country_list","region",[
// iso =>$exp_country
// ]);
// $region = $region_data[0];

// //get country in same region
// $country_data = $db->select("country_list","iso",[
// region =>$region
// ]);




$db->update("country_brief",["set9a"=>$set9a,"set9"=>$set9],["AND"=>["economy"=>$exp_country,"year"=>$year]]);

?>