<?php 
///imp_country = import country ส่งมาเป็นรหัสประเทศ 3 ตัว
//exp_country = export country ส่งมาเป็นรหัสประเทศ 3 ตัว
//source_country = source countrh ส่งมาเป็นรหัสประเทศ 3 ตัว
//year = ปี ส่งเป็น ค.ศ. 2017

require_once('connection.php');
require_once('sector_data.php');

$imp_country = $_GET['imp_country'];
$exp_country = $_GET['exp_country'];
$year = $_GET['year'];
$sector = $_GET['sector'];
$tableName = strtolower($exp_country) . "_" . $year;


//Imported content in exports

if($sector == 0){
  $sql = "select sum(value) as sum from " . $tableName . " where imp_country = '" . $imp_country. "' and variable = 'fva_yl' and ( source_country NOT IN ('sea', 'nca', 'sswa', 'enea', 'pac', 'ap', 'euz', 'eur', 'apta', 'saarc', 'nafta', 'mercosur', 'cptpp', 'rcep', 'apec', 'lac', 'pac_alliance', 'fealac', 'bimstec', 'wld')) group by imp_country";
  // $sql = "select sum(value) as sum from " . $tableName  . " where imp_country = '" . $imp_country . "' and variable = 'fva_yl' ";
  // echo $sql;
  $value = $db->query($sql)->fetchAll();
// $value = $db->sum($tableName,"value",[
//     imp_country => $imp_country,
//     variable => ['fva_yl']
// ]);

} else {


  $sql = "select sum(value) as sum from " . $tableName . " where imp_country = '" . $imp_country. "' and variable = 'fva_yl' and exp_sector ='". $sector_data[$sector] ."' and ( source_country NOT IN ('sea', 'nca', 'sswa', 'enea', 'pac', 'ap', 'euz', 'eur', 'apta', 'saarc', 'nafta', 'mercosur', 'cptpp', 'rcep', 'apec', 'lac', 'pac_alliance', 'fealac', 'bimstec', 'wld')) ";
  $value = $db->query($sql)->fetchAll();


//  $value = $db->sum($tableName,"value",[
//     imp_country => $imp_country,
//     variable => ['fva_yl'],
//     exp_sector=>$sector_data[$sector],
//   ]);  
}
$result['ImportedContent'] = round($value[0][0],2);

//Gross exports to

if($sector == 0){
$value = $db->sum($tableName,"value",[
    imp_country => $imp_country,
    variable => ['total_export']
]);

} else {
 $value = $db->sum($tableName,"value",[
    imp_country => $imp_country,
    variable => ['total_export'],
    exp_sector=>$sector_data[$sector],
  ]);  
}
$result['grossExport'] = round($value,2);


 echo json_encode($result);
?>
