<?php 
///imp_country = import country ส่งมาเป็นรหัสประเทศ 3 ตัว
//exp_country = export country ส่งมาเป็นรหัสประเทศ 3 ตัว
//source_country = source countrh ส่งมาเป็นรหัสประเทศ 3 ตัว
//year = ปี ส่งเป็น ค.ศ. 2017

require_once('connection.php');
require_once('sector_data.php');

$exp_country = $_GET['exp_country'];
$year = $_GET['year'];
$sector = $_GET['sector'];
// $exp_country = 'VNM';
// $year = 2017;
// $sector = 0;
$tableName = strtolower($exp_country) . "_" . $year;


//Imported content in exports

if($sector == 0){
  $sql = "select sum(value) as sum from " . $tableName . " where (variable = 'DVA_INTrex1'  or variable = 'DVA_INTrex2' or variable = 'DVA_INTrex3' or variable = 'RDV_FIN1' or variable = 'RDV_FIN2' or variable = 'RDV_INT' ) and ( imp_country NOT IN ('sea', 'nca', 'sswa', 'enea', 'pac', 'ap', 'euz', 'eur', 'apta', 'saarc', 'nafta', 'mercosur', 'cptpp', 'rcep', 'apec', 'lac', 'pac_alliance', 'fealac', 'bimstec', 'wld')) ";

   $value = $db->query($sql)->fetchAll();
// $value = $db->sum($tableName,"value",[
//     variable => ['DVA_INTrex1','DVA_INTrex2','DVA_INTrex3','RDV_FIN1', 'RDV_FIN2', 'RDV_INT']
// ]);


} else {
  $sql = "select sum(value) as sum from " . $tableName . " where (variable = 'DVA_INTrex1'  or variable = 'DVA_INTrex2' or variable = 'DVA_INTrex3' or variable = 'RDV_FIN1' or variable = 'RDV_FIN2' or variable = 'RDV_INT' ) and exp_sector = '" .$sector_data[$sector] . "'and ( imp_country NOT IN ('sea', 'nca', 'sswa', 'enea', 'pac', 'ap', 'euz', 'eur', 'apta', 'saarc', 'nafta', 'mercosur', 'cptpp', 'rcep', 'apec', 'lac', 'pac_alliance', 'fealac', 'bimstec', 'wld')) ";
  $value = $db->query($sql)->fetchAll();
//  $value = $db->sum($tableName,"value",[
//     variable => ['DVA_INTrex1','DVA_INTrex2','DVA_INTrex3','RDV_FIN1', 'RDV_FIN2', 'RDV_INT'],
//     exp_sector=>$sector_data[$sector],
//   ]);  
}


$result['contribution'] = round($value[0][0],2);

//Gross exports to

if($sector == 0){
  $sql = "select sum(value) as sum from " . $tableName . " where variable = 'total_export'  and ( imp_country NOT IN ('sea', 'nca', 'sswa', 'enea', 'pac', 'ap', 'euz', 'eur', 'apta', 'saarc', 'nafta', 'mercosur', 'cptpp', 'rcep', 'apec', 'lac', 'pac_alliance', 'fealac', 'bimstec', 'wld')) ";
   $value = $db->query($sql)->fetchAll();
}
else {
$sql = "select sum(value) as sum from " . $tableName . " where variable = 'total_export' and exp_sector ='". $sector_data[$sector] ."' and ( imp_country NOT IN ('sea', 'nca', 'sswa', 'enea', 'pac', 'ap', 'euz', 'eur', 'apta', 'saarc', 'nafta', 'mercosur', 'cptpp', 'rcep', 'apec', 'lac', 'pac_alliance', 'fealac', 'bimstec', 'wld')) ";
$value = $db->query($sql)->fetchAll();
}

//  $value = $db->sum($tableName,"value",[
//     variable => ['total_export'],
//     exp_sector=>$sector_data[$sector],
//   ]);  

$result['grossExport'] = round($value[0][0],2);


 echo json_encode($result);
?>
